//
//  Board.swift
//  Board
//
//  Created by Rochdi Aries on 11/09/2021.
//

import Foundation
import SwiftUI

struct Board {
    let WIDTH = 19
    let HEIGHT = 19
    var placedStones = [Int: Player]()
    
    func stonesPlaced() -> Int {
        return placedStones.count
    }
    
    mutating func place(_ intesec: Intersection, _ player: Player) throws {
        let location = try makeLocationFrom(intesec)

        guard placedStones[location] == nil else {
            throw BoardError.AlreadyOccupyLocation
        }
        
        placedStones[location] = player
    }

    func getPlayerFor(_ intesec: Intersection) throws -> Player {
        let location = try makeLocationFrom(intesec)
        
        guard let placedStone = placedStones[location] else {
            return .empty
        }
        
        return placedStone
    }
    
    private func makeLocationFrom(_ intesec: Intersection) throws -> Int {
        guard !isIntersecOutBound(intesec) else {
            throw BoardError.outOfBoundLocation
        }
        
        return intesec.column * WIDTH + intesec.row
    }
    
    func isIntersecOutBound(_ intesec: Intersection) -> Bool {
        return isOutOfBoundForColumn(intesec.column) || isOutOfBoundForRow(intesec.row)
    }
    
    func isOutOfBoundForColumn(_ column: Int) -> Bool {
        return column < 0 || column >= WIDTH
    }
    
    private func isOutOfBoundForRow(_ row: Int) -> Bool {
        return row < 0 || row >= HEIGHT
    }
}

enum BoardError: Error {
    case outOfBoundLocation
    case AlreadyOccupyLocation
}


enum Player {
    case white
    case black
    case empty
}

struct Intersection {
    let column: Int
    let row: Int
}
