//
//  GomukuRules.swift
//  GomukuRules
//
//  Created by Rochdi Aries on 11/09/2021.
//

import Foundation

class GomokuRules {
    
    func isWinFor(_ board: Board) -> Bool {
        var placedWhiteStonesInARow = 0
        var placedBlackStonesInARow = 0
        for row in 0..<board.HEIGHT {
            for column in 0..<board.WIDTH {
                let currIntersec = Intersection(column: column, row: row)
                if let placedStone = try? board.getPlayerFor(currIntersec){
                    if placedStone == .white {
                        placedWhiteStonesInARow += 1
                    }
                    
                    if placedStone == .black {
                        placedBlackStonesInARow += 1
                    }
                    
                }
            }
            
            if placedWhiteStonesInARow >= 5 || placedBlackStonesInARow >= 5 {
                break
            } else {
                placedWhiteStonesInARow = 0
                placedBlackStonesInARow = 0
            }
        }
       
        
        
        return placedWhiteStonesInARow >= 5 || placedBlackStonesInARow >= 5
    }
}
