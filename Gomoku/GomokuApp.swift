//
//  GomokuApp.swift
//  Gomoku
//
//  Created by Rochdi Aries on 11/09/2021.
//

import SwiftUI

@main
struct GomokuApp: App {
    var body: some Scene {
        WindowGroup {
            GridView()
        }
    }
}
