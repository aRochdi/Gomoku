//
//  MyColors.swift
//  MyColors
//
//  Created by Rochdi Aries on 20/09/2021.
//

import SwiftUI

extension Color {
    static var boardColor: Color {
        Color("BoardColor")
    }
}
