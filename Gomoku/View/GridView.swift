//
//  GridView.swift
//  GridView
//
//  Created by Rochdi Aries on 14/09/2021.
//

import SwiftUI

class GridViewModel: ObservableObject {
    @Published var board = Board()
    let rules = GomokuRules()
    var playerTurn: Player = .white
    
    func placeStone(_ intersection: Intersection) throws {
        try board.place(intersection, playerTurn)
        playerTurn = playerTurn == .white ? .black : .white
    }
}

struct GridView: View {
    private let numberOfIntersection = 19
    private let lineWidth: CGFloat = 1.0
    private var boardScreenFrame: CGFloat { UIScreen.main.bounds.width - 20 }
    private let stoneSize: CGFloat = 7
    
    @StateObject private var viewModel: GridViewModel = GridViewModel()
    
    var body: some View {
        ZStack {
            StaticBoardView.build(numberOfIntersection: numberOfIntersection, lineWidth: lineWidth, screenWidth: boardScreenFrame)
            ButtonGridView.build(numberOfIntersection: numberOfIntersection, screenWidth: boardScreenFrame, stoneSize: stoneSize, gridViewModel: viewModel)
        }
    }
}

struct GridView_Previews: PreviewProvider {
    static var previews: some View {
        GridView()
    }
}
