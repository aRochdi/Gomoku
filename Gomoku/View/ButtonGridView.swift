//
//  ButtonGridView.swift
//  ButtonGridView
//
//  Created by Rochdi Aries on 19/09/2021.
//

import SwiftUI

struct ButtonGridView: View {
    let buttonSize: CGFloat
    let stoneSize: CGFloat
    let buttGridSize: CGFloat
    @ObservedObject var gridViewModel: GridViewModel
    
    // MARK: - Builder
    static func build(numberOfIntersection: Int, screenWidth: CGFloat, stoneSize: CGFloat, gridViewModel: GridViewModel) -> ButtonGridView {
        let numberOfGridLines = numberOfIntersection + 2
        let lineSpace = screenWidth / CGFloat(numberOfGridLines)
        let squareSize: CGFloat = lineSpace - 2
        let buttonSize = lineSpace + 1
        let buttGridSize: CGFloat = screenWidth - (squareSize * 2)

        return ButtonGridView(buttonSize: buttonSize, stoneSize: stoneSize, buttGridSize: buttGridSize, gridViewModel: gridViewModel)
    }
    
    // MARK: - Body
    var body: some View {
        VStack(spacing: 0) {
            ForEach(0..<19) { column in
                HStack(spacing: 0) {
                    ForEach(0..<19) { row in
                        let intersection = Intersection(column: column, row: row)
                        Button {
                            didIntersectionClicked(intersection)
                        } label: {
                            if let color = getIntersectionColorFor(intersection) {
                                Circle()
                                    .fill(color)
                                    .frame(width: stoneSize, height: stoneSize)
                            } else {
                                Rectangle()
                                    .fill(.clear)
                            }
                        }
                        .frame(width: buttonSize, height: buttonSize)
                    }
                }
            }
        }
        .frame(width: buttGridSize, height: buttGridSize)
    }
    
    // MARK: - Helpers
    private func didIntersectionClicked(_ intersection: Intersection) {
        do {
            try gridViewModel.placeStone(intersection)
        } catch {
            print("Error for intersection: \(intersection.column):\(intersection.row) - \(error.localizedDescription)")
        }
    }
    
    private func getIntersectionColorFor(_ intersection: Intersection) -> Color? {
        guard let state = try? gridViewModel.board.getPlayerFor(intersection) else {
            return nil
        }
        
        switch state {
        case .white:
            return .white
        case .black:
            return .black
        case .empty:
            return nil
        }
    }
}

struct ButtonGridView_Previews: PreviewProvider {
    static var previews: some View {
        ButtonGridView(buttonSize: 20, stoneSize: 20, buttGridSize: UIScreen.main.bounds.width - 20, gridViewModel: GridViewModel())
    }
}
