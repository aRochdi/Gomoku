//
//  StaticBoardView.swift
//  StaticBoardView
//
//  Created by Rochdi Aries on 18/09/2021.
//

import SwiftUI

struct StaticBoardView: View {
    private let numberOfGridLines: Int
    private let lineWidth: CGFloat
    private let lineSpacing: CGFloat
    private var screenWidth: CGFloat
    
    // MARK: - Builder
    static func build(numberOfIntersection: Int, lineWidth: CGFloat, screenWidth: CGFloat) -> StaticBoardView {
        let numberOfGridLines = numberOfIntersection + 2
        let lineSpacing = screenWidth / CGFloat(numberOfGridLines)
        return StaticBoardView(numberOfGridLines: numberOfGridLines,
                               lineWidth: lineWidth,
                               lineSpacing: lineSpacing,
                               screenWidth: screenWidth)
    }
    
    
    // MARK: - body
    var body: some View {
        ZStack {
            HStack(spacing: lineSpacing) {
                ForEach(0..<numberOfGridLines) { index in
                    Rectangle()
                        .foregroundColor(getLineColorFor(index))
                        .frame(width: lineWidth, height: screenWidth)

                }
            }
            VStack(spacing: lineSpacing) {
                ForEach(0..<numberOfGridLines) { index in
                    Rectangle()
                        .foregroundColor(getLineColorFor(index))
                        .frame(width: screenWidth, height: lineWidth)

                }
            }
            
        }
        .frame(width: screenWidth, height: screenWidth)
        .border(.black, width: lineWidth)
        .background(Color.boardColor)
    }
    
    // MARK: - Helpers
    private func getLineColorFor(_ index: Int) -> Color {
        isInTheEgdeOfTheBoardFor(index) ? .black : .clear
    }
    
    private func isInTheEgdeOfTheBoardFor(_ index: Int) -> Bool {
        return index != 0 && index != numberOfGridLines - 1
    }
}

struct StaticBoardView_Previews: PreviewProvider {
    static var previews: some View {
        StaticBoardView.build(numberOfIntersection: 19, lineWidth: 1, screenWidth: UIScreen.main.bounds.width - 20)
            .frame(width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.width)
    }
}
