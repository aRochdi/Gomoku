//
//  BoardTests.swift
//  BoardTests
//
//  Created by Rochdi Aries on 11/09/2021.
//

import XCTest
@testable import Gomoku

class BoardTests: XCTestCase {
    
    var board: Board!
    
    override func setUp() {
        super.setUp()
        
        board = Board()
    }

    func testShouldBoardEmptyAtInitialisation() {
        // GIVEN
        // a new Board
        // WHEN & THEN
        XCTAssertEqual(0, board.stonesPlaced())
    }
    
    func testShouldPlaceOneWhiteStone() throws {
        // GIVEN
        // a new Board
        let intersection = Intersection(column: 0, row: 0)

        // WHEN
        try board.place(intersection, .white)
        
        // THEN
        XCTAssertEqual(1, board.stonesPlaced())
    }
    
    func testShouldPlaceOneBlackStone() throws {
        // GIVEN
        // a new Board
        let intersection = Intersection(column: 0, row: 0)

        // WHEN
        try board.place(intersection, .white)
        
        // THEN
        XCTAssertEqual(1, board.stonesPlaced())
    }
    
    
    func testShouldReturnRightPlayerForCorrespondingPlacedStone() throws {
        // GIVEN
        // a new Board
        let whiteStoneIntersec = Intersection(column: 0, row: 0)
        let backStoneIntersec = Intersection(column: 0, row: 1)

        // WHEN
        try board.place(whiteStoneIntersec, .white)
        let white = try board.getPlayerFor(whiteStoneIntersec)
        
        try board.place(backStoneIntersec, .black)
        let black = try board.getPlayerFor(backStoneIntersec)

        
        // THEN
        XCTAssertEqual(.white, white)
        XCTAssertEqual(.black, black)
    }
    
    func testShouldKnowAboutEmptyIntersection() throws {
        // GIVEN
        // a new Board
        let intersection = Intersection(column: 0, row: 0)
        // WHEN
        let emptyIntersec = try board.getPlayerFor(intersection)
        
        // THEN
        XCTAssertEqual(.empty, emptyIntersec)
    }

    func testShouldNotAbleToPlaceForAnIntersectionWithAtLeastOneCoordNegativeOrBoth() throws {
        // GIVEN
        let negativeColAndRowInter = Intersection(column: -1, row: -1)
        let negativeColAndCorrectRowInter = Intersection(column: -1, row: 1)
        let correctColAndNegativeRowInter = Intersection(column: 1, row: -1)

        // WHEN & THEN
        XCTAssertThrowsError(try board.place(negativeColAndRowInter, .white))
        XCTAssertThrowsError(try board.place(negativeColAndCorrectRowInter, .white))
        XCTAssertThrowsError(try board.place(correctColAndNegativeRowInter, .white))
    }
    
    func testShouldNotAbleToPlaceForAnIntersectionWithAtLeastOneCoordOutOfBoundOrBoth() throws {
        // GIVEN
        let outOfBoundColAndRowInter = Intersection(column: board.WIDTH, row: board.HEIGHT)
        let outOfBoundColAndCorrectRowInter = Intersection(column: board.WIDTH, row: 1)
        let correctColAndoutOfBoundRowInter = Intersection(column: 1, row: board.HEIGHT)

        // WHEN & THEN
        XCTAssertThrowsError(try board.place(outOfBoundColAndRowInter, .white))
        XCTAssertThrowsError(try board.place(outOfBoundColAndCorrectRowInter, .white))
        XCTAssertThrowsError(try board.place(correctColAndoutOfBoundRowInter, .white))
    }
    
    
    func testShouldNotAbleToPlaceForAnOccupyIntersection() throws {
        // GIVEN
        let intersection = Intersection(column: 0, row: 0)
        
        // WHEN
        try board.place(intersection, .white)
        
        // THEN
        XCTAssertThrowsError(try board.place(intersection, .black))
        XCTAssertThrowsError(try board.place(intersection, .white))
    }

}
