//
//  GomokuRulesTests.swift
//  GomokuRulesTests
//
//  Created by Rochdi Aries on 11/09/2021.
//

import XCTest

class GomokuRulesTests: XCTestCase {
    var board: Board!
    var rules: GomokuRules!
    
    override func setUp() {
        super.setUp()
        
        board = Board()
        rules = GomokuRules()
    }
    
    func testSouldNotBeAWinForEmptyBoard() {
        // GIVEN
        // new Board
        // nes GomokuRules
        
        // WHEN & THEN
        XCTAssertFalse(rules.isWinFor(board))
    }
    
    func testShouldNotBeAWinForFourStoneInTheFirstRow() throws {
        // GIVEN
        // new Board
        // nes GomokuRules
        let row = 0
        
        // WHEN
        for i in 0..<4 {
            let currentInter = Intersection(column: i, row: row)
            try board.place(currentInter, .white)
        }
        
        // THEN
        XCTAssertFalse(rules.isWinFor(board))
    }
    
    func testShouldBeAWinForFiveStoneInTheFirstRow() throws {
        // GIVEN
        // new Board
        // nes GomokuRules
        let row = 0
        
        // WHEN
        for i in 0..<5 {
            let currentInter = Intersection(column: i, row: row)
            try board.place(currentInter, .white)
        }
        
        // THEN
        XCTAssertTrue(rules.isWinFor(board))
    }
    
    func testShouldBeAWinForFiveBlackStoneInTheFirstRow() throws {
        // GIVEN
        // new Board
        // nes GomokuRules
        let row = 0
        
        // WHEN
        for i in 0..<6 {
            let currentInter = Intersection(column: i, row: row)
            try board.place(currentInter, .black)
        }
        
        // THEN
        XCTAssertTrue(rules.isWinFor(board))
    }
    
    func testShouldNotBeAWinForThreeWhiteStonesAndTwoBlackStones() throws{
        // GIVEN
        // new Board
        // nes GomokuRules
        let row = 0
        
        // WHEN
        for i in 0..<3 {
            let currentInter = Intersection(column: i, row: row)
            try board.place(currentInter, .white)
        }
        
        for i in 3..<5 {
            let currentInter = Intersection(column: i, row: row)
            try board.place(currentInter, .black)
        }
        
        // THEN
        XCTAssertFalse(rules.isWinFor(board))
    }
    
    func testShoudBeAWinForFiveStonesInAnyRow() throws {
        for row in 0..<board.HEIGHT {
            for col in 0..<5 {
                try board.place(Intersection(column: col, row: row), .white)
            }
            XCTAssertTrue(rules.isWinFor(board))
            board = Board()
        }
    }
}
